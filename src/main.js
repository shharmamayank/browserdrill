
const screen = document.getElementById("window");
function renderData(todos, userData) {
  console.log(userData)
  userData.map((obj) => {
    let getId = obj.id;
    // Element creation
    const div = document.createElement("div");
    const h1 = document.createElement("h1");
    const headText = document.createTextNode(obj.name);

    h1.appendChild(headText);
    div.appendChild(h1);

    todos.map((element) => {
      let todos = element.slice(0, 7);
      todos.map((todo) => {
        if (todo.userId === getId) {
          const p = document.createElement("p");
          const paraText = document.createTextNode("[] " + todo.title);
          p.appendChild(paraText);
          div.appendChild(p);
        }
      });
    });
    screen.appendChild(div);
  });
}
