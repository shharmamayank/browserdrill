function getUsers(api) {
  return fetch(api).then(data => {
    return data.json()
  }).then(data => {
    UserDataId(data)
  }).catch((error) => {
    console.log(error)
  })
}

getUsers("https://jsonplaceholder.typicode.com/users")
function UserDataId(userData) {
  const userId = userData.map(data => {
    return (data.id)
  })
  let getUserTodo = userId.map(data => {
    return `https://jsonplaceholder.typicode.com/todos?userId=${data}`
  })
  Promise.all(
    getUserTodo.map((url) => fetch(url).then((response) => response.json()))
  ).
    then((data) => {
      renderData(data, userData);
    })
    .catch((error) => {
      console.log(error);
    });
}

