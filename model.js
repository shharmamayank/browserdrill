function getUsers() {
    fetch("https://jsonplaceholder.typicode.com/users")
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        createURLArray(data);
      })
      .catch((error) => {
        console.log(error.message);
      });
  }
  
  getUsers();
  
  function createURLArray(usersData) {
    const allIds = usersData.map((obj) => {
      return obj.id;
    });
  
    // now adding urls
  
    const address = allIds.map((obj) => {
      return `https://jsonplaceholder.typicode.com/todos?userId=${obj}`;
    });
  
    Promise.all(
      address.map((url) => fetch(url).then((response) => response.json()))
    )
      .then((data) => {
        render(data, usersData);
      })
      .catch((error) => {
        console.log(error);
      });
  }
  