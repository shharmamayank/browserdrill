
const screen = document.getElementById("window");

function render(todos, userData) {
  userData.map((obj) => {
    let id = obj.id;

    // Element creation
    const tab = document.createElement("div");

    const h1 = document.createElement("h1");

    const headText = document.createTextNode(obj.name);

    h1.appendChild(headText);

    tab.appendChild(h1);

    todos.forEach((element) => {
      let todos = element.slice(0, 7);

      todos.map((todo) => {
        if (todo.userId === id) {
          const para = document.createElement("p");
          const paraText = document.createTextNode("[] " + todo.title);
          para.appendChild(paraText);
          tab.appendChild(para);
        }
      });
    });
    screen.appendChild(tab);
  });
}
